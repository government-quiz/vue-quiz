# Stack

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

https://emojitwo.github.io/ <br>
https://css-tricks.com/swipeable-card-stack-using-vue-js-and-interact-js/ <br>
https://vuejs.org/ <br>
https://fontawesome.com/v4.7.0/ <br>
https://buefy.org/documentation <br>

https://www.flaticon.com/free-icon/constitution_1939567?term=constitution&page=1&position=2 <br>
https://www.flaticon.com/free-icon/crown_1657088?term=king&page=1&position=4 <br>
https://www.flaticon.com/free-icon/soldier_1693485?term=military&page=1&position=19 <br>
https://www.flaticon.com/free-icon/politics_1651600 <br>
https://www.flaticon.com/free-icon/hands_470232?term=vote&page=1&position=36 <br>
https://www.flaticon.com/free-icon/capitol_1666090 <br>
https://www.flaticon.com/free-icon/king_2014293 <br>
https://www.flaticon.com/free-icon/dictatorship_2038665?term=dictatorship&page=1&position=1 <br>
https://www.flaticon.com/free-icon/boss_265674?term=leader&page=1&position=2 <br>
https://www.flaticon.com/free-icon/anarchy_2097309 <br>