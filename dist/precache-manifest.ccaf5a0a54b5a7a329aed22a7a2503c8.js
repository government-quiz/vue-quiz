self.__precacheManifest = (self.__precacheManifest || []).concat([
  {
    "revision": "f1a14794f41fbefe1a1e",
    "url": "/css/app.935acb5f.css"
  },
  {
    "revision": "4c1d2a8a06954918ce45",
    "url": "/css/chunk-vendors.413fea29.css"
  },
  {
    "revision": "674f50d287a8c48dc19ba404d20fe713",
    "url": "/fonts/fontawesome-webfont.674f50d2.eot"
  },
  {
    "revision": "af7ae505a9eed503f8b8e6982036873e",
    "url": "/fonts/fontawesome-webfont.af7ae505.woff2"
  },
  {
    "revision": "b06871f281fee6b241d60582ae9369b9",
    "url": "/fonts/fontawesome-webfont.b06871f2.ttf"
  },
  {
    "revision": "fee66e712a8a08eef5805a46892932ad",
    "url": "/fonts/fontawesome-webfont.fee66e71.woff"
  },
  {
    "revision": "6b3ea1564213438c2d9b72fc5693df57",
    "url": "/img/absolute_monarchy.6b3ea156.svg"
  },
  {
    "revision": "a3d4751c8f9d3f73d3fdff0462c73fb6",
    "url": "/img/agree.a3d4751c.svg"
  },
  {
    "revision": "2996ea623e581d00553693b7285e44c3",
    "url": "/img/anarchy.2996ea62.svg"
  },
  {
    "revision": "c2333493053b4117df1f5f6f7fb79863",
    "url": "/img/bottom_feature.c2333493.svg"
  },
  {
    "revision": "c885c7891ea698f12c2ccf7a1dc7dd26",
    "url": "/img/checks_and_balance.c885c789.svg"
  },
  {
    "revision": "a579a89be57b326d8612cdc9f0bb3d37",
    "url": "/img/confirm.a579a89b.svg"
  },
  {
    "revision": "37d5251f635a7edbcee8cf9b18444ecf",
    "url": "/img/conservative.37d5251f.svg"
  },
  {
    "revision": "ce02e68844bdc49afddc79c130dd9dd7",
    "url": "/img/constitutional_monarchy.ce02e688.svg"
  },
  {
    "revision": "0bae0e42b1b40d1ee2638352bf98a99f",
    "url": "/img/direct_democracy.0bae0e42.svg"
  },
  {
    "revision": "7e2b6533470d2ee89e7bfa5078da819d",
    "url": "/img/disagree.7e2b6533.svg"
  },
  {
    "revision": "13e81caaf080b107fd49a991df9012e1",
    "url": "/img/equality.13e81caa.svg"
  },
  {
    "revision": "912ec66d7572ff821749319396470bde",
    "url": "/img/fontawesome-webfont.912ec66d.svg"
  },
  {
    "revision": "86ec353b9cc0d99b4b0e034cae571fd1",
    "url": "/img/freedom.86ec353b.svg"
  },
  {
    "revision": "f7509acd71251ee2609915c4116a6ace",
    "url": "/img/freedom_of_speech.f7509acd.svg"
  },
  {
    "revision": "6b3ea1564213438c2d9b72fc5693df57",
    "url": "/img/governments/absolute_monarchy.svg"
  },
  {
    "revision": "2996ea623e581d00553693b7285e44c3",
    "url": "/img/governments/anarchy.svg"
  },
  {
    "revision": "ce02e68844bdc49afddc79c130dd9dd7",
    "url": "/img/governments/constitutional_monarchy.svg"
  },
  {
    "revision": "0bae0e42b1b40d1ee2638352bf98a99f",
    "url": "/img/governments/direct_democracy.svg"
  },
  {
    "revision": "f30f84c22a0549cccd7705df2b54fc1d",
    "url": "/img/governments/half_direct_democracy.svg"
  },
  {
    "revision": "262b7d1622da17411b136b7b85276cbd",
    "url": "/img/governments/indirect_democracy.svg"
  },
  {
    "revision": "e3b5d644c7597743999b1f06b21c4611",
    "url": "/img/governments/military_dictatorship.svg"
  },
  {
    "revision": "9e93ceba33f75818e4b04233714fa1dd",
    "url": "/img/governments/parliamentary_monarchy.svg"
  },
  {
    "revision": "ce65daaa59c756d3f57124fec84281de",
    "url": "/img/governments/party_dictatorship.svg"
  },
  {
    "revision": "8be6c167b1ba67df3c90238dbbcf879b",
    "url": "/img/governments/theocracy.svg"
  },
  {
    "revision": "f30f84c22a0549cccd7705df2b54fc1d",
    "url": "/img/half_direct_democracy.f30f84c2.svg"
  },
  {
    "revision": "78b7da66683ea2a130574be35f9c3af5",
    "url": "/img/hierarchy.78b7da66.svg"
  },
  {
    "revision": "800ee0dc64d72749f9a8ad6aac142d0b",
    "url": "/img/high_efficiency.800ee0dc.svg"
  },
  {
    "revision": "b97bbaaa7bb8377053a8c2a50aa93963",
    "url": "/img/hire.b97bbaaa.svg"
  },
  {
    "revision": "890bbf40c06431c1de96f171dae4bf8e",
    "url": "/img/home/conference.svg"
  },
  {
    "revision": "f7a2ba41ed0eb978e13d50e5fe9bf988",
    "url": "/img/home/devices.svg"
  },
  {
    "revision": "760539cc01b4bafede640e500b0144ce",
    "url": "/img/home/done.svg"
  },
  {
    "revision": "a579a89be57b326d8612cdc9f0bb3d37",
    "url": "/img/home/features/confirm.svg"
  },
  {
    "revision": "b97bbaaa7bb8377053a8c2a50aa93963",
    "url": "/img/home/features/hire.svg"
  },
  {
    "revision": "86bc429e115c9955fdda9bae38bdcd7a",
    "url": "/img/home/features/percentages.svg"
  },
  {
    "revision": "b691f4d2c0774dc97b1ddeb43f0461a8",
    "url": "/img/home/features/reading.svg"
  },
  {
    "revision": "402666de4aca1a0ddd8c198c4a27822e",
    "url": "/img/home/government/government_wave.svg"
  },
  {
    "revision": "17b56c2e67ff87774d4840ee4c284127",
    "url": "/img/home/online_test.svg"
  },
  {
    "revision": "cc6918915e76862715d791ff238f3641",
    "url": "/img/home/pattern.svg"
  },
  {
    "revision": "b8f2a1b5d22359f70e3ae238604b7708",
    "url": "/img/home/preferences.svg"
  },
  {
    "revision": "1d2e0e0cd606465ffa87ce50c508d699",
    "url": "/img/home/quiz.svg"
  },
  {
    "revision": "58c8d05b78ed26d5756418cef00823b6",
    "url": "/img/home/quiz/blob_1.svg"
  },
  {
    "revision": "11a2746c51be1ec5d42ebb0aa13c20f2",
    "url": "/img/home/quiz/blob_2.svg"
  },
  {
    "revision": "74ae98aabfe4a18209ff21e9b3b626f8",
    "url": "/img/home/quiz/blob_3.svg"
  },
  {
    "revision": "dc215fba880cf75d7826a15d26552b57",
    "url": "/img/home/quiz/blob_4.svg"
  },
  {
    "revision": "c891003c11ceaa627061a3acea64f4f9",
    "url": "/img/home/quiz/blob_5.svg"
  },
  {
    "revision": "30c3c952837fd27f856026f15170f994",
    "url": "/img/home/quiz/quiz_wave.svg"
  },
  {
    "revision": "3df5c1ce6ed734d0de04ec6cd381be37",
    "url": "/img/home/teaching - Copy.svg"
  },
  {
    "revision": "2e590485fd5d0a23759a36036f5f40b7",
    "url": "/img/home/teaching.svg"
  },
  {
    "revision": "b3b7330e69e5ebf4894640f841bda1f4",
    "url": "/img/images.b3b7330e.svg"
  },
  {
    "revision": "262b7d1622da17411b136b7b85276cbd",
    "url": "/img/indirect_democracy.262b7d16.svg"
  },
  {
    "revision": "6e9e2258e2e38a4dcecfaecd0dca176f",
    "url": "/img/law_and_order.6e9e2258.svg"
  },
  {
    "revision": "03648e0adb9e8fe1a03232d6ff160d24",
    "url": "/img/low_efficiency.03648e0a.svg"
  },
  {
    "revision": "c2333493053b4117df1f5f6f7fb79863",
    "url": "/img/meta/bottom_feature.svg"
  },
  {
    "revision": "b3b7330e69e5ebf4894640f841bda1f4",
    "url": "/img/meta/images.svg"
  },
  {
    "revision": "ec1489db33dd637120d729e138f72c6e",
    "url": "/img/meta/open_source.svg"
  },
  {
    "revision": "7d07860be8d5b745d82660a27d375283",
    "url": "/img/meta/page_not_found.svg"
  },
  {
    "revision": "6319ff32241ed99a8f6fa640ceaf4f23",
    "url": "/img/meta/site_content.svg"
  },
  {
    "revision": "5514f9c509b01c3f8f9c49347c992508",
    "url": "/img/meta/team.svg"
  },
  {
    "revision": "c983ac8d8f1c6a2b4ec7a04d31b2ad09",
    "url": "/img/meta/top.svg"
  },
  {
    "revision": "82d98b2a1ea6de8e8da8caf11f3a9e76",
    "url": "/img/meta/top_feature.svg"
  },
  {
    "revision": "7e408c813cb2d5841027ac5e61b79cbc",
    "url": "/img/meta/wave.svg"
  },
  {
    "revision": "e3b5d644c7597743999b1f06b21c4611",
    "url": "/img/military_dictatorship.e3b5d644.svg"
  },
  {
    "revision": "9e93ceba33f75818e4b04233714fa1dd",
    "url": "/img/parliamentary_monarchy.9e93ceba.svg"
  },
  {
    "revision": "ce65daaa59c756d3f57124fec84281de",
    "url": "/img/party_dictatorship.ce65daaa.svg"
  },
  {
    "revision": "86bc429e115c9955fdda9bae38bdcd7a",
    "url": "/img/percentages.86bc429e.svg"
  },
  {
    "revision": "b8f2a1b5d22359f70e3ae238604b7708",
    "url": "/img/preferences.b8f2a1b5.svg"
  },
  {
    "revision": "ce16dbf9715c862b18d56f47739b2b2b",
    "url": "/img/progressive.ce16dbf9.svg"
  },
  {
    "revision": "c885c7891ea698f12c2ccf7a1dc7dd26",
    "url": "/img/questions/checks_and_balance.svg"
  },
  {
    "revision": "37d5251f635a7edbcee8cf9b18444ecf",
    "url": "/img/questions/conservative.svg"
  },
  {
    "revision": "13e81caaf080b107fd49a991df9012e1",
    "url": "/img/questions/equality.svg"
  },
  {
    "revision": "86ec353b9cc0d99b4b0e034cae571fd1",
    "url": "/img/questions/freedom.svg"
  },
  {
    "revision": "f7509acd71251ee2609915c4116a6ace",
    "url": "/img/questions/freedom_of_speech.svg"
  },
  {
    "revision": "78b7da66683ea2a130574be35f9c3af5",
    "url": "/img/questions/hierarchy.svg"
  },
  {
    "revision": "800ee0dc64d72749f9a8ad6aac142d0b",
    "url": "/img/questions/high_efficiency.svg"
  },
  {
    "revision": "6e9e2258e2e38a4dcecfaecd0dca176f",
    "url": "/img/questions/law_and_order.svg"
  },
  {
    "revision": "03648e0adb9e8fe1a03232d6ff160d24",
    "url": "/img/questions/low_efficiency.svg"
  },
  {
    "revision": "ce16dbf9715c862b18d56f47739b2b2b",
    "url": "/img/questions/progressive.svg"
  },
  {
    "revision": "98e98b948117ebbc269e690ae08358fd",
    "url": "/img/questions/speech.svg"
  },
  {
    "revision": "052ee2f3f5c941353d345010ed42990c",
    "url": "/img/quiz-logo.png"
  },
  {
    "revision": "1d2e0e0cd606465ffa87ce50c508d699",
    "url": "/img/quiz.1d2e0e0c.svg"
  },
  {
    "revision": "6319ff32241ed99a8f6fa640ceaf4f23",
    "url": "/img/site_content.6319ff32.svg"
  },
  {
    "revision": "98e98b948117ebbc269e690ae08358fd",
    "url": "/img/speech.98e98b94.svg"
  },
  {
    "revision": "2e590485fd5d0a23759a36036f5f40b7",
    "url": "/img/teaching.2e590485.svg"
  },
  {
    "revision": "5514f9c509b01c3f8f9c49347c992508",
    "url": "/img/team.5514f9c5.svg"
  },
  {
    "revision": "86f247929f7164ab3d42249adae3bb39",
    "url": "/img/text_logo.86f24792.png"
  },
  {
    "revision": "86f247929f7164ab3d42249adae3bb39",
    "url": "/img/text_logo.png"
  },
  {
    "revision": "8be6c167b1ba67df3c90238dbbcf879b",
    "url": "/img/theocracy.8be6c167.svg"
  },
  {
    "revision": "c983ac8d8f1c6a2b4ec7a04d31b2ad09",
    "url": "/img/top.c983ac8d.svg"
  },
  {
    "revision": "82d98b2a1ea6de8e8da8caf11f3a9e76",
    "url": "/img/top_feature.82d98b2a.svg"
  },
  {
    "revision": "7e408c813cb2d5841027ac5e61b79cbc",
    "url": "/img/wave.7e408c81.svg"
  },
  {
    "revision": "c9e2f216e7df3d747ec01ca3a7c1e5f9",
    "url": "/index.html"
  },
  {
    "revision": "f1a14794f41fbefe1a1e",
    "url": "/js/app.d8ea79a3.js"
  },
  {
    "revision": "4c1d2a8a06954918ce45",
    "url": "/js/chunk-vendors.4ff6dfb9.js"
  },
  {
    "revision": "ff5bc87d0bc34ce121205dd87d5299d1",
    "url": "/manifest.json"
  },
  {
    "revision": "735ab4f94fbcd57074377afca324c813",
    "url": "/robots.txt"
  }
]);