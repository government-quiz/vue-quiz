import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/components/Home.vue';
import GovernmentsCards from '@/components/government/GovernmentsCards.vue';
import GovernmentDetails from '@/components/government/GovernmentDetails.vue';
import StackList from '@/components/stack/StackList.vue';
import StackResults from '@/components/stack/StackResults.vue';
import Imprint from '@/components/meta/Imprint.vue';
import PrivacyPolicy from '@/components/meta/PrivacyPolicy.vue';
import StackLicense from '@/components/meta/StackLicense.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/governments',
        name: 'Governments',
        component: GovernmentsCards,
    },
    {
        path: '/government/:id',
        name: 'GovernmentDetails',
        component: GovernmentDetails,
    },
    {
        path: '/stack',
        name: 'StackList',
        component: StackList,
    },
    {
        path: '/results',
        name: 'StackResults',
        component: StackResults,
    },
    {
        path: '/imprint',
        name: 'Imprint',
        component: Imprint,
    },
    {
        path: '/privacy',
        name: 'PrivacyPolicy',
        component: PrivacyPolicy,
    },
    {
        path: '/license',
        name: 'StackLicense',
        component: StackLicense,
    },
];

const router = new VueRouter({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
