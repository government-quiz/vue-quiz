import { Government } from '@/models/government';

export interface System {
    id: number;
    name: string;
    description: string;
    governments: Government[];
}
