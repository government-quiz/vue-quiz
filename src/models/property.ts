export interface Property {
    left_title: string;
    right_title: string;
    value: number;
    description: string;
}
