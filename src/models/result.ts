import { Government } from '@/models/government';

export interface Result {
    id: number;
    government: Government;
    value: number;
}
