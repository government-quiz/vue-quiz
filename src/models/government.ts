import { Property } from './property';

export interface Government {
    id: number;
    title: string;
    subtitle: string;
    description: string;
    properties: Property[];
    img: string;
}
