// dini frage chasch mer i dere struktur geh

interface Question {
    id: number; // immer ufstiigend
    text: string; // (die eigentlichi frag)
    agree: number[]; // 10 integer, wo inere resultat array zumene government resultat addiert werde sötted
    disagree: number[]; // 10 integer, wo inere resultat array zumene government resultat addiert werde sötted
    answered: boolean; // dasch so eppis vo mer, da muesch eifach immer false ahgeh
}

class DiniMuetter {
    // und eso söttis denne biispielswiis usgseh:

    questions: Question[] = [
        {
            id: 1,
            text: 'Ich bin e frag 1',
            agree: [3, 2, 1, 3, 6, 10, 10, 10, 10, 0],
            disagree: [6, 7, 7, 7, 5, 3, 0, 4, 0, 10],
            answered: false,
        }, //alles pls mit kommas separiere so wie da https://www.w3schools.com/js/js_json_arrays.asp
        {
            id: 2,
            text: 'Ich bin nomol e frag xdd',
            agree: [8, 9, 4, 8, 10, 1, 2, 3, 2, 0],
            disagree: [5, 6, 3, 5, 5, 5, 5, 0, 10, 10],
            answered: false,
        },
    ];
}
