export interface Question {
    id: number;
    text: string;
    img: string;
    agree: number[];
    disagree: number[];
    answered: boolean;
}
