import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from '@/router';
import store from './vuex/store';

Vue.config.productionTip = false;

// Main Sass file
import '@/assets/sass/main.scss';

// Font awesome icons (4.7)
import 'font-awesome/css/font-awesome.min.css';

// -- BUEFY --
import Buefy from 'buefy';
Vue.use(Buefy, {
    defaultIconPack: 'fa', // Maybe change to fas (new one)
});

// Define the base api url
Vue.prototype.BACKEND_BASE_URL = process.env.VUE_APP_BACKEND_BASE_URL;
Vue.prototype.APP_BASE_URL = process.env.VUE_APP_BASE_URL;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
