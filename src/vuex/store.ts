import Vue from 'vue';
import Vuex from 'vuex';

// Import different modules
import question from './modules/question/question';
import system from './modules/system/system';
import results from './modules/results/result';

// Utils
import utils from './utils/store';

// import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
    // plugins: [createPersistedState()],

    modules: {
        system,
        utils,
        question,
        results,
    },
});
