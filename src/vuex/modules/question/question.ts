import Vue from 'vue';
import Vuex, { MutationTree, ActionTree } from 'vuex';
import { Question } from '@/models/question';

Vue.use(Vuex);

interface State {
    questions: Question[];
}

const mutations: MutationTree<State> = {};

const actions: ActionTree<State, any> = {};

const getters = {
    questions(currentState: State) {
        return currentState.questions;
    },
};

const state: State = {
    questions: [
        {
            id: 1,
            text:
                'Die Bevölkerung sollte wenig Mitsprache bei politischen Entscheiden haben. Es ist besser, wenn eine Elite diese Entscheide übernimmt.',
            img: 'checks_and_balance.svg',
            agree: [3, 2, 1, 3, 6, 10, 10, 10, 10, 0],
            disagree: [7, 8, 9, 7, 4, 0, 0, 0, 0, 10],
            answered: false,
        },
        {
            id: 2,
            text: 'Ich finde es wichtig, dass das Volk wichtige politische Entscheidungen trifft.',
            img: 'speech.svg',
            agree: [7, 8, 9, 7, 4, 0, 0, 0, 0, 10],
            disagree: [3, 2, 1, 3, 6, 10, 10, 10, 10, 0],
            answered: false,
        },
        {
            id: 3,
            text:
                'Es muss strikte Regeln geben, an welche man sich halten muss. Das ist wichtig für das Gedeihen und Prosperieren einer Gesellschaft.',
            img: 'law_and_order.svg',
            agree: [5, 3, 3, 3, 6, 7, 10, 6, 10, 0],
            disagree: [5, 7, 7, 7, 4, 3, 0, 4, 0, 10],
            answered: false,
        },
        {
            id: 4,
            text:
                'Freiheiten zu haben, das bedeutet, uneingeschränkt von Regeln und Gesetzen zu leben, dies ist der erstrebenswerte Zustand des Menschen.',
            img: 'freedom.svg',
            agree: [5, 7, 7, 7, 4, 3, 0, 4, 0, 10],
            disagree: [5, 3, 3, 3, 6, 7, 10, 6, 10, 0],
            answered: false,
        },
        {
            id: 5,
            text:
                'Altbewährte Werte sind die Grundbausteine der Gesellschaft Diese zu erhalten und nicht jedem neuen Trend nachzugeben ist wichtig.',
            img: 'conservative.svg',
            agree: [5, 4, 7, 5, 8, 9, 5, 10, 0, 0],
            disagree: [5, 6, 3, 5, 2, 1, 5, 0, 10, 10],
            answered: false,
        },
        {
            id: 6,
            text:
                'Wir leben in einer sich rasant entwickelnden Welt, die Staatsform sollte sich somit schnell auf neue Ereignisse und Situationen einstellen können.',
            img: 'progressive.svg',
            agree: [5, 6, 3, 5, 2, 1, 5, 0, 10, 10],
            disagree: [5, 4, 7, 5, 8, 9, 5, 10, 0, 0],
            answered: false,
        },
        {
            id: 7,
            text:
                'Politik zu machen ist ein langwieriger Prozess, eine Lösung zu finden kann dauern und das ist gut so.',
            img: 'low_efficiency.svg',
            agree: [8, 10, 4, 8, 10, 1, 2, 3, 2, 0],
            disagree: [2, 0, 6, 2, 0, 9, 8, 7, 8, 10],
            answered: false,
        },
        {
            id: 8,
            text:
                'Auch wichtige Entscheide sollten schnellstmöglich getroffen werden. Lange Diskussionen führen oft zu nichts.',
            img: 'high_efficiency.svg',
            agree: [2, 0, 6, 2, 0, 9, 8, 7, 8, 10],
            disagree: [8, 10, 4, 8, 10, 1, 2, 3, 2, 0],
            answered: false,
        },
        {
            id: 9,
            text:
                'Findest du es gut, dass für Staatsgeschäfte eine Elite verantwortlich ist, die das Wissen und die Kapazität hat, wichtige Entscheide zu fällen?',
            img: 'hierarchy.svg',
            agree: [0, 0, 0, 4, 7, 10, 8, 10, 3, 0],
            disagree: [10, 10, 10, 6, 3, 0, 2, 0, 7, 10],
            answered: false,
        },
        {
            id: 10,
            text: 'Alle Menschen sind gleich, egal welche Herkunft oder Werdegänge sie haben.',
            img: 'equality.svg',
            agree: [10, 10, 10, 6, 3, 0, 2, 0, 7, 10],
            disagree: [0, 0, 0, 4, 7, 10, 8, 10, 3, 0],
            answered: false,
        },
    ],
};

const module = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};

export default module;
