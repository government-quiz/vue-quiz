import Vue from 'vue';
import Vuex, { MutationTree, ActionTree, GetterTree } from 'vuex';
import { System } from '@/models/system';

Vue.use(Vuex);

interface State {
    systems: System[];
}

const mutations: MutationTree<State> = {};

const actions: ActionTree<State, any> = {};

const getters: GetterTree<State, any> = {
    systems(currentState: State) {
        return currentState.systems;
    },
};

const state: State = {
    systems: [
        {
            id: 1,
            name: 'Demokratien',
            description: 'Demokratie ist cool',
            governments: [
                {
                    id: 1,
                    title: 'Indirekte Demokratie',
                    subtitle: 'Die Repräsentative Demokratie',
                    description:
                        'Bei der Indirekten Demokratie oder auch Repräsentative Demokratie wählt das Volk Stellvertreter. Diese Stellvertreter sind meistens für eine bestimmte Zeit im Amt und während dieser Zeit bestimmen sie über Gesetze. Das Volk ist in dieser Staatsform bloss berechtigt, die Stellvertreter zu wählen, nicht aber über Gesetze abzustimmen.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 3,
                            description:
                                'In der Indirekten Demokratie kann das Volk zwar nicht direkt mitbestimmen, aber sie können entscheiden, wer für sie Gesetze macht. Ein gutes Beispiel für eine Indirekte Demokratie sind die Vereinigten Staaten. In der USA ist es wichtig, dass alle bei der Politik des Landes mitbestimmen können. Die Punktzahl ist hier aber trotzdem höher als bei den anderen Demokratien, weil das Volk nicht direkt mitsprechen kann, sondern einfach Vertreter wählt.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 5,
                            description:
                                'Hier liegt die Punktzahl sehr in der Mitte, da es selbst in der USA von Staat zu Staat sehr variiert. Zum Beispiel wenn man den Konsum von Marihuana betrachtet, gibt es in einigen Staaten ein striktes Verbot und in einigen Staaten ist es erlaubt für Menschen ab dem 21. Lebensjahr. Es variiert in diesem Beispiel aber nicht nur von Staat zu Staat, sondern auch in verschiedenen Themen, zum Beispiel ist der Besitz oder sogar das Tragen von Waffen sehr locker reguliert, während Abtreibung in vielen Staaten sehr stark eingeschränkt ist.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 5,
                            description:
                                'Auch hier ist die Punktzahl in der Mitte gehalten, weil es bei dem Paradebeispiel der USA sehr starke Schwankungen im eigenen Land gibt. Allgemein werden sehr viele Südstaaten als eher konservativ angesehen. Tradition ist dort sehr hoch angesehen, währenddessen sind sehr viele andere Staaten als Progressiv bekannt. Nur schon, wenn man sich ansieht, wie viel Innovation von der USA kommt, ist offensichtlich, dass sie sich mit der Zukunft beschäftigen aber sie hängen trotzdem noch an vielen Traditionen. ',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 8,
                            description:
                                'Demokratien haben im Allgemeinen eine eher tiefe Effizienz, da sich sehr viele Menschen zusammen auf etwas einigen müssen. In den USA müssen die meisten Beschlüsse zwar nicht durch das Volk, aber sie müssen angenommen werden im Parlament. Das Problem hierbei ist, dass die USA nur zwei Parteien im Parlament haben, die in sehr vielen Punkten gegenteilige Ansichten haben, deswegen kann es schwer sein, einen Beschluss durchzubringen. ',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 0,
                            description:
                                'In den USA ist es gesetzlich geregelt, dass jeder Mensch gleichbehandelt wird und, dass jeder Mensch vor dem Gesetz gleich ist. Ausserdem wird auch per Gesetz jeder Bürger vor staatlicher Willkür geschützt durch das Legalitätsprinzip. Das Legalitätsprinzip besagt, dass ein Staat nur machen kann, was im Gesetz steht.',
                        },
                    ],
                    img: 'indirect_democracy.svg',
                },
                {
                    id: 2,
                    title: 'Halbdirekte Demokratie',
                    subtitle: 'Die Schweizerische Demokratie',
                    description:
                        'In der Halb-direkten Demokratie gibt es vom Volk gewählte Repräsentanten, welche Parlamente oder Räte bilden, und die Möglichkeit für die Bevölkerung direkt Einfluss auf die Verfassung zu nehmen durch Initiativen oder Referenden. Charakteristisch für diese Staatsform sind das Mitspracherecht, die vielen Freiheiten, eine tiefe Effizienz darin Veränderungen umzusetzen und eine Gleichheit aller Bürger vor dem Gesetz.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 2,
                            description:
                                'Das Volk kann über einen Grossteil der Besetzung der politischen Ämter entscheiden, jedoch nicht ausnahmslos. Einige Ämter werden von Vertretern des Volkes gewählt, folglich hat das Volk teilweise nur einen indirekten Einfluss. Mit Initiativen und Referenden kann das Volk ebenfalls einen direkten Einfluss auf die Politik nehmen. Für Volksinitiativen und Referenden gibt es klare Richtlinien, welche erfüllt werden müssen, um das Ganze rechtskräftig zu machen.  Aber auch für dieses regulierte Vorgehen wird eine Mehrheit des Volkes benötigt was zusammen mit den Positionen, welche nicht von den Bürgern gewählt werden können, zu einer leichten Tendenz weg von einer kompletten Mitsprache hin zu der Staatsgewalt führt, was zu einer Gewichtung von Zwei führt.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 3,
                            description:
                                'Bestehende Gesetze müssen eingehalten werden, was eher auf Law and Order deutet. Jedoch hat das Volk das Recht, Gesetze zu verändern, wodurch es sich zusätzliche Freiheiten einräumen kann.  Da man dafür aber gesetzlich geregelte Schritte machen muss, besteht keine komplette Freiheit, sondern es gibt eine klare Einschränkung, was uns zu einer Drei auf der Skala bringt.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 4,
                            description:
                                'In der Schweiz ereignet sich momentan ein Linksrutsch zugunsten der SP, den Grünen und der GLP. All diese Parteien wollen Fortschritt unteranderem im Bereich der Klimapolitik erreichen, folglich wollen sie von den teils klimaschädigenden Traditionen wegkommen. Sprich es gibt eine leichte Tendenz zum Progressiven hin. Jedoch ist die allgemeine Politlandschaft eher ausgeglichen was uns zu einer Vier auf der Skala führt.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 10,
                            description:
                                'Die Wahlen von Politikern dauern stets eine Weile. Politiker verschwenden während dieses Prozesses ihre Zeit für den Wahlkampf und kümmern sich weniger um Staatsanliegen. Die Bürger können auch über bestimmte Vorlagen, welche vom Parlament ausgearbeitet wurden, abstimmen. In diesem Fall wird ebenfalls Wahlkampf betrieben, was sehr zeitaufwendig ist, und man muss dem Volk Zeit geben, um sich zu entscheiden, was zu einer Verzögerung der Anpassung führt. Des Weiteren besteht die Möglichkeit, dass das Volk bei Gesetzesänderungen ein Referendum ergreift und die Veränderung ablehnen kann. Dies würde bedeuten, dass die gesamte Ausarbeitung der Vorlage umsonst gewesen wäre. Folglich ist eine Halb-direkte Demokratie vollkommen ineffizient.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 0,
                            description:
                                'Die Gesetze stellen sicher, dass es keine Bevorzugung einzelner Personen oder Organisationen gibt, was für gleiche Freiheiten sorgt. Die Gleichheit aller Menschen vor dem Gesetz ist ein Grundsatz für jegliche Art einer Demokratie. Auf der Skala ist es folglich eine Null für eine komplette Gleichheit.',
                        },
                    ],
                    img: 'half_direct_democracy.svg',
                },
                {
                    id: 3,
                    title: 'Direkte Demokratie',
                    subtitle: 'Der Wille des Volks',
                    description:
                        'In dieser Staatsform können die Bürger direkt über Vorlagen abstimmen. Es existiert kein Parlament oder sonstige politische Ämter, welche vom Volk gewählt werden. Typisch für die direkte Demokratie ist das Mitspracherecht, die vielen Freiheiten und eine Gleichheit aller Bürger vor dem Gesetz.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 1,
                            description:
                                'In dieser Staatsform hat die Mehrheit des Volkes die Macht. Die Möglichkeit, ein Referendum oder eine Volksinitiative einzureichen, ist eine Möglichkeit sich direkt in die Politik einzubringen. Gegen eine komplette Mitsprache sprechen die Gesetze des bestehenden Staates. Für Volksinitiativen und Referenden gibt es beispielsweise klare Richtlinien, welche erfüllt werden müssen, um das Ganze rechtskräftig zu machen. Da eine Mehrheit der Bevölkerung benötigt ist, um Gesetze zu verändern, resultiert nur in einer geringen Tendenz zur Staatsgewalt, dies bringt uns schlussendlich auf eine Eins.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 3,
                            description:
                                'Bestehende Gesetze müssen eingehalten werden, was eher auf Law and Order deutet. Jedoch hat das Volk das Recht, Gesetze zu verändern, wodurch es sich zusätzliche Freiheiten einräumen kann.  Da man dafür aber gesetzlich geregelte Schritte machen muss, besteht keine komplette Freiheit, sondern es gibt eine klare Einschränkung was uns zu einer drei auf der Skala bringt.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 7,
                            description:
                                'Es sind wenig Informationen überliefert über die Progressivität oder Konservativität des antiken Griechenlandes, wo diese Staatsform ihren Uhrsprung hat. Das macht eine Aussage schwierig. Jedoch kann man bei einem heutigen Beispiel der direkten Demokratie, dem Kanton Appenzell Innerrhoden, sehen, dass dieser konservativ ist. Ob dies allgemein auf diese Staatsform zutrifft oder ob dies eine Ausnahme ist, ist schwierig zu sagen, da es kaum direkte Demokratien gibt. Obwohl das eine Beispiel Konservativ ist, ist es auf der Skala eine Sieben. Wenn es mehr Beispiele geben würde, so könnten diese um einiges progressiver sein. Mit einer Sieben wird der potentielle Mittelwert am besten getroffen.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 4,
                            description:
                                'Das Volk stimmt direkt über Sachfragen und Gesetze ab. Dies macht die direkte Demokratie um einiges effizienter als andere Formen der Demokratie, denn die Wahl von Repräsentanten wird weggelassen. Das Erreichen einer Mehrheit kann jedoch viel Zeit kosten, was zu einer starken Tendenz zur tiefen Effizienz führt und folglich eine vier auf der Skala ist.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 0,
                            description:
                                'Die Gesetze stellen sicher, dass es keine Bevorzugung einzelner Personen oder Organisationen gibt, was für gleiche Freiheiten sorgt. Die Gleichheit aller Menschen vor dem Gesetz ist ein Grundsatz für jegliche Art einer Demokratie. Auf der Skala ist es folglich eine Null für eine komplette Gleichheit.',
                        },
                    ],
                    img: 'direct_democracy.svg',
                },
            ],
        },
        {
            id: 2,
            name: 'Monarchien',
            description: 'Monarchie ist cool',
            governments: [
                {
                    id: 4,
                    title: 'Parlamentarische Monarchie',
                    subtitle: 'Die Repräsentative Monarchie',
                    description:
                        'In einer Parlamentarischen Monarchie hat der Monarch keine eigentliche Funktion mehr. Der Monarch ist in diesem Fall meistens nur für die Repräsentation im Ausland da. Die Gesetze werden in dieser Staatsform von einem Parlament gemacht, das vom Volk gewählt wurde.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 3,
                            description:
                                'Bei einer Parlamentarischen Monarchie hat das Volk die Möglichkeit zu wählen und über einige Dinge abzustimmen. Das Meiste wird aber von einem Parlament bestimmt, das von dem Volk gewählt wird. Ein gutes Beispiel ist Grossbritannien, wo das Volk über etwas abstimmen kann aber dann im Parlament bestimmt wird wie es wirklich umgesetzt wird.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 3,
                            description:
                                'Bei der Parlamentarischen Monarchie hat man viel weniger Staatsgewalt als bei den anderen Monarchien. Dies kommt daher, dass es keinen Monarchen gibt, der tatsächlich einen Einfluss hat. Das hat zur Folge, dass niemand mithilfe von Staatsgewalt seine Position sichern muss oder kann.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 5,
                            description:
                                'Hier eignet sich Grossbritannien als bestes Beispiel. Grossbritannien ist allgemein eher ein progressives Land. Trotzdem haben gibt es hier eine Punktzahl in der Mitte. Das liegt daran, da man an einigen Stellen noch heute eine besondere Wichtigkeit von Tradition erkennen kann. Ein gutes Beispiel hier ist die Monarchin, die immer noch im Amt ist. Sie hat zwar keine Macht aber ist trotzdem wegen der Tradition immer noch im Amt.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 8,
                            description:
                                'Die Effizienz liegt sehr tief, da sehr viele Leute mitbestimmen können. Um einen Entscheid zu fällen muss das Parlament sich einigen und in manchen Fällen auch das Volk noch abstimmen. Zurzeit ist perfekt beobachtbar, zu was dies führen kann, wenn man sich den Brexit anschaut. Dieser muss durchgehend verschoben werden, weil sich das Parlament nicht einigen kann.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 4,
                            description:
                                'Diese Punktzahl ist leicht speziell, denn vor Gesetz sind alle Menschen gleich ausser die Monarchen Familie. Es existieren daher eigentlich keine Schichten, aber es gibt trotzdem einen Adel. Die Menschen sind aber alle vor Willkür geschützt durch das Legalitätsprinzip.',
                        },
                    ],
                    img: 'parliamentary_monarchy.svg',
                },
                {
                    id: 5,
                    title: 'Konstitutionelle Monarchie',
                    subtitle: 'Die Übergangsform der Monarchien',
                    description:
                        'In einer Konstitutionellen Monarchie gibt es einen Monarchen oder eine Monarchen Familie, die mit einer Volksvertretung, meist in Form eines Parlaments, gemeinsam über Gesetze entscheiden muss. Dazu ist der Monarch durch Gesetze eingeschränkt, an die er sich halten muss. Dieses System hat das grosse Problem, dass sich das Parlament und der Monarch bei jeder Entscheidung einigen müssen und demnach eine Seite alles aufhalten kann.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 6,
                            description:
                                'Bei der Konstitutionellen Monarchie ist es eher schwer zu entscheiden, wie das Mitspracherecht für das Volk ist, da es starke Unterschiede in verschiedenen Ländern und Zeiten gab. Es gibt hierbei für das Volk nur die Möglichkeit das Parlament zu wählen. Dieses entscheidet aber zusammen mit dem Monarchen über Beschlüsse. Das schwere bei der Punktzahl ist hier, dass das Machtverhältnis zwischen Parlament und Monarch sehr stark variiert. Deswegen eine eher mittlere Punktzahl.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 6,
                            description:
                                'Auch hier eine Punktzahl eher in der Mitte des Spektrums da die Konstitutionelle Monarchie eher ein Übergang von der Absoluten zu der Parlamentarischen Monarchie ist. Die Menschen bekamen mehr Freiheiten aber die Staatsgewalt war dennoch sehr hoch, da viel Macht in vielen Fällen immer noch bei dem Monarchen lag. Dieser braucht die Staatsgewalt um seine Position zu sichern.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 8,
                            description:
                                'Hier hat es eine sehr hohe Punktzahl. Dies ist aus dem einfachen Grund, dass diese Staatsform eher eine der Vergangenheit ist und damals die meisten Leute sehr konservativ eingestellt waren. Alleine schon die wichtige Rolle der Religion und der Präsenz eines Monarchen zeigen eine sehr traditionelle und konservative Weltansicht.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 10,
                            description:
                                'Die konstitutionelle Monarchie hat eine extrem niedrige Effizienz, da, um zu einem Beschluss zu kommen, sich der Monarch und das Parlament einigen müssen. Das Problem hierbei ist, dass diese meistens in einem Machtkampf zu einander stehen und sich deswegen nicht einigen wollen. Dadurch braucht das Treffen von Entscheidungen sehr lange und oft passiert gar nichts.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 7,
                            description:
                                'Hier haben wir eine eher hohe Punktzahl, da es zwar mehr Gleichheit gab in dieser Staatsform als in der Absoluten Monarchie, aber Menschen immer noch alles andere als gleich angesehen wurden. Besonders Frauen durften nicht abstimmen und in einigen Ländern auch Männer nur unter bestimmten Konditionen, zum Beispiel musste man in einigen Fällen Landbesitzer sein.',
                        },
                    ],
                    img: 'constitutional_monarchy.svg',
                },
                {
                    id: 6,
                    title: 'Absolute Monarchie',
                    subtitle: 'Ein Königsreich',
                    description:
                        'In einer Absoluten Monarchie hat ein Monarch oder eine Monarchen Familie die komplette Kontrolle über die Gesetze, die Gerichte und das Militär. Es gibt also keine Gewaltentrennung in dieser Staatsform. Ausserdem müssen sich die Monarchen nicht an Gesetze halten und können Gesetze neu machen oder abschaffen, wie sie wollen. Die Monarchen sind oftmals an der Macht, weil sie angeblich von Gott gewählt worden sind.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 10,
                            description:
                                'In einer Absoluten Monarchie hat das Volk kein Mitspracherecht, es wird alles von dem Monarchen beschlossen. Aus diesem Grund ist die Absolute Monarchie natürlich eine Zehn.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 7,
                            description:
                                'In einer Absoluten Monarchie ist Law and Order sehr hoch, weil der Monarch natürlich sicherstellen muss, dass er an der Macht bleibt. Dazu kommt, dass in der Regel eine Monarchie auf Religion basiert, die den Menschen vorschreibt, wie sie leben sollen. Religion ist natürlich nicht direkt ein Gesetz aber in der Zeit, in der es die meisten Absoluten Monarchien gab, war es etwa gleich wichtig wie Gesetze und wer etwas gegen die Kirche sagte, wurde trotzdem bestraft. Aus diesem Grund kann man hier nicht die höchste Punktzahl geben, aber mit einer Sieben trotzdem eine hohe.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 9,
                            description:
                                'Die Absolute Monarchie ist eine eher ältere Staatsform. Im Sinne, dass es sie heutzutage nicht mehr so oft gibt. Dies liegt unter anderem daran, dass sie sehr konservativ ist. Sie beruht stark auf Religion, Tradition und der Weltansicht, dass es Menschen gibt die besser sind als andere.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 1,
                            description:
                                'Die Absolute Monarchie hat eine sehr hohe Effizienz, da der Monarch Entscheide fällen kann, ohne die Zustimmung einer anderen Person einholen zu müssen. Es gibt also keine Verhandlungen oder Abstimmungen, die den Entscheid verzögern und dadurch hat die Absolute Monarchie natürlich eine sehr hohe Effizienz.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 10,
                            description:
                                'Die Absolute Monarchie ist so ziemlich das beste Beispiel für eine Hierarchie. Menschen werden in verschiedene Schichten eingeordnet und es ist sehr schwer oder sogar unmöglich, aus einer Schicht aufzusteigen. Die Menschen sind also überhaupt nicht gleich vor dem Gesetz und auch komplett der Willkür bei Gerichtsbeschlüssen ausgesetzt.',
                        },
                    ],
                    img: 'absolute_monarchy.svg',
                },
            ],
        },
        {
            id: 3,
            name: 'Diktaturen',
            description: 'Ich bin eine Diktatur',
            governments: [
                {
                    id: 7,
                    title: 'Militärdiktatur',
                    subtitle: 'Autoritäres Regime unter der Führung vom Militär',
                    description:
                        'In der Militärdiktatur liegt die Macht bei einem General oder einem Rat von militärischen Persönlichkeiten. Diese haben die Macht durch einen Militärputsch errungen. Für diese Staatsform typisch ist Staatsgewalt, strenge Gesetze, eine hohe Effizienz darin, Veränderungen umzusetzen und starke Hierarchien.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 10,
                            description:
                                'Die Bevölkerung hat keinerlei Mitsprache bei politischen Entscheidungen. Die Militärdiktatur beginnt oft mit einem Putsch durch einzelne Heeresführer und ist nicht durch eine Mehrheit legitimiert. Militärdiktaturen sind häufig durch die Unterdrückung von politischen Gegnern gekennzeichnet.1 Dies kann von Drohungen über Folter bis zu der Ermordung von politisch andersgestimmten Personen gehen. Solche Massnahmen zu vollziehen ist ein Einfaches, da alle drei Gewalten vereint sind und der Diktator absolut herrscht. Folglich hat man kein Mitspracherecht und man riskiert sogar sein Leben, wenn man seine Meinung äussert.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 10,
                            description:
                                'Militärorganisationen im Allgemeinen sind streng strukturiert durch Regeln und Vorschriften, welche zwar Freiheiten der Soldaten einschränken, jedoch das Funktionieren und Fortbestehen der Organisation als Ganzes garantieren. Es ist naheliegend, dass bei einem Militärputsch diese Ideologie in die Verfassung einfliesst. Dies kann man am Beispiel der Militärdiktatur in Brasilien sehen, welche bürgerliche Freiheiten eingeschränkt haben um das Land besser unter Kontrolle zu bekommen. Dies macht es zu einer klaren Zehn auf der Skala.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 5,
                            description:
                                'Hier gehen die Beispiele weit auseinander. In Chile wurden während der Militärdiktatur unter General Pinchot radikale marktorientierte Wirtschaftsreformen eingeführt mit dem Ziel, das Land durch Neuerung voranzubringen. Hier will ganz klar Fortschritt erreicht werden, was es progressiv macht und es zu einer null oder eins machen würde. Andererseits haben wir aber beispielsweise Argentinien, welches als ultranationalistisches Militärregime beschrieben wird. Die Regierung beschreibt sich selbst als ein nach christlich-konservativen Werten ausgerichteter Staat. Deshalb wäre es eine neun oder zehn. Da es keine klaren Tendenzen gibt, wird die Militärdiktatur in der Mitte der Skala, bei einer Fünf, klassiert.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 2,
                            description:
                                'Militärdiktaturen sind von Wenigen regiert. Dies führt dazu, dass Gesetzesänderungen und Veränderungen im Allgemeinen schnell umgesetzt werden können. Es kann jedoch zu Unterschieden kommen, wie viele die Diktatur regieren, je mehr es sind desto länger kann es dauern, bis es zu einer Entscheidung kommt. Daher hat es nicht die höchstmögliche Effizienz sondern ist eine Zwei.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 8,
                            description:
                                'Militärorganisationen haben starke Hierarchien, welche den unterschiedlichen Rängen unterschiedliche Rechte zusprechen. Ein Soldat hat beispielsweise nicht so viele Rechte und Freiheiten wie ein General. Das zeigt eine klare Ungleichheit in der Militärdiktatur auf. Ein Aufstieg ist möglich, abhängig von den Fähigkeiten und persönlichen Verbindungen zu höheren Positionen in der Hierarchie. Das führt zu einer Abschwächung der Ungleichheit. Ebenfalls ist man nicht durch Gott oder sonst eine höhere Macht in seine Position gesetzt worden, was ebenfalls zu einer Abschwächung führt und uns im Endeffekt auf eine Acht auf der Skala bringt.',
                        },
                    ],
                    img: 'military_dictatorship.svg',
                },
                {
                    id: 8,
                    title: 'Theokratie',
                    subtitle: 'Staatsform mit religiösem Oberhaupt',
                    description:
                        'In einer Theokratie herrscht ein religiöses Oberhaupt. Dieses hat auf die anderen Staatsorgane direkten oder indirekten Einfluss. Charakteristisch für diese Staatsform ist die Vermischung von Religion und Staat. Staatsgewalt, konservative Ansichten, eine starke Hierarchie und eine hohe Effizienz um Veränderungen umzusetzen sind typisch für die Theokratie.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 10,
                            description:
                                'In der Theokratie wird das Staatsoberhaupt von Gott erwählt und nicht vom Volk. Bei den anderen wichtigen Positionen im Staat besteht ebenfalls selten ein Mitspracherecht. Da es keine Trennung von Religion und Staat gibt, vermischen sich Gesetze der Regierung und religiöse Gebote. Vorschriften des Glaubens wurden vor langer Zeit festgesetzt und können nicht verändert werden. Die einzige Ausnahme wäre das Staatsoberhaupt, welches einen bestehenden Text neu interpretieren könnte, jedoch entzieht sich dies wieder dem Einfluss des Volkes, folglich haben die Bürger kein Mitspracherecht.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 10,
                            description:
                                'Führung nutzt aus, dass die Gesellschaft Druck auf einzelne ausübt. Dies kann selbst im gleichen Land von Region zu Region stark variieren. Im Iran haben Frauen teilweise sehr wenige Rechte und müssen zuhause bleiben, andernorts haben sie mehr Freiheiten und dürfen arbeiten. Nach dem Gesetz sind Mann und Frau gleichberechtigt – unter der Berücksichtigung der islamischen Prinzipien – dies benachteiligt die Frau gleichwohl. Dazu kommt, dass in der Gesellschaft häufig gegenseitig Druck ausgeübt wird. Man behält seine Mitmenschen im Auge, ob sie sich auch ja an alle Vorschriften und Traditionen halten. Wenn man gegen diese verstösst, verliert man Achtung und Respekt, so hält man sich an Normen, selbst wenn es kein explizites Gesetz gibt. Da der Staat nicht alles direkt reglementiert und in einigen Aspekten auch mehr Rechte gewährt, wird es auf der Skala in der Mitte mit einer Tendenz zu Law and Order platziert.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 5,
                            description:
                                'Weil Staat und Religion eins sind, werden alle Reformen auf religiöse Prinzipien abgestimmt. Da sich die religiösen Prinzipien nicht verändern werden, kann sich der Staat als ganzes auch nur mit Schwierigkeiten von alten Traditionen lösen. Oft gibt es Verbote, welche in der Bevölkerung auf Widerstand stossen, wie zum Beispiel das Frauenverbot in Fussballstadien im Iran. In diesem Fall hat der Selbstmord eines Fussballfans die Aufmerksamkeit des Westens, unteranderem der Fifa, geweckt. Unter dem Druck westlicher Aktionen wurde das Verbot im Oktober 2019 abgeschafft. Theokratien sind sehr konservativ, können sich aber – wenn auch nur ungern – anpassen, um die Ruhe in der Bevölkerung zu wahren. Deshalb ist es am konservativen Ende der Skala mit einer leichten Tendenz zur Progressivität.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 2,
                            description:
                                'In der Theokratie gibt es meist nicht eine Person die absolut regiert, sondern es gibt auch noch Räte oder Parlamente. Da das Oberhaupt viele der anderen Regierungsorgane teilweise oder sogar ganze ernannt hat, hat es viel Einfluss und kann Leute einsetzten, die auf seiner Linie sind und ihn unterstützen. Es gibt jedoch immer noch einige wenige Leute, welche er nicht direkt beeinflussen kann und welche Prozesse verlangsamen können. Da die wichtigen Positionen aber vom Staatsoberhaupt bestimmt werden, ist die mögliche Opposition nur klein, was zu einer minimalen Verminderung der Effizienz führt. Dies bringt uns auf der Skala zu einer drei.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 8,
                            description:
                                'Das Staatsoberhaupt, welches eine einzige Person oder eine Priesterschaft sein kann, wurde von Gott erwählt. Dies stellt bereits wenige Individuen über alle anderen. Dazu kommt, dass es innerhalb einer Religion ebenfalls Abstufungen zwischen den verschiedenen Ämtern gibt und da Staat und Religion eins sind, bekleiden sie auch im politischen System höhere Positionen. Gläubige müssen dieses System unterstützen, denn falls sie dagegen wären, würden sie ihrem Gott widersprechen da diese Hierarchie gottgewollt ist.',
                        },
                    ],
                    img: 'theocracy.svg',
                },
                {
                    id: 9,
                    title: 'Parteiendiktatur',
                    subtitle: 'Parteiherrschaft',
                    description:
                        'Die Parteiendiktatur wird von einer Einheitspartei regiert. Häufig werden Scheinwahlen durchgeführt, um den Anschein zu wecken, eine Demokratie zu sein. Merkmale sind die Staatsgewalt, strenge Gesetze, progressive Ansichten und eine hohe Effizienz, um Veränderungen umzusetzen.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 10,
                            description:
                                'Eine Parteiendiktatur gibt vor, eine Demokratie zu sein, jedoch gibt es wenig Gemeinsamkeiten mit einer wirklichen Demokratie. Beispielsweise kann nur eine Partei gewählt werden, da keine Opposition zugelassen wird. Es kann vorkommen, dass es gleichwohl zu Wahlen kommt. Doch das sind nur Scheinwahlen, welche keinen Einfluss haben, da der Sieger schon von vornherein feststeht. Folglich ist es nur Staatsgewalt und der Bürger hat keinerlei Mitsprache.',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 10,
                            description:
                                'China ist wohl eines der extremsten Beispiele. Sicherheit steht über der persönlichen Freiheit. Um mögliche Gefahren zu vermeiden werden beispielsweise muslimische Minderheiten in Umerziehungslager inhaftiert, in welchen ihnen extremistische Gedanken ausgetrieben werden sollen. Nebst diesem Vorgehen setzt die Regierung auf Überwachung, unter anderem mit dem Gebrauch von Gesichtserkennung. Man will die Kontrolle über die Bevölkerung bewahren. Ein weiteres Beispiel ist Nordkorea. Hier werden Medien vom Staat kontrolliert und Versammlungen müssen genehmigt werden. Kurzum: es gibt keinerlei Freiheiten, sondern nur Kontrolle.',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 0,
                            description:
                                'Linke Parteien sind tendenziell progressiv. Nichts ist mehr links als Kommunismus. Die Einheitsparteien haben fast ausnahmslos Kommunistische Partei (KP) im Namen oder haben kommunistische Werte. Dies zeigt eine klare Tendenz zu progressiv. China ist auch hier ein gutes Beispiel. Es ist offen für Veränderungen, das kann man in ihrem schnellen Wirtschaftswachstum sehen, welchen sie Reformen zu verdanken haben.11 Parteiendiktaturen sind deshalb klar progressiv.',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 2,
                            description:
                                'Obwohl das Volk nicht mit einbezogen wird, was Prozesse sehr viel schneller macht, kosten Scheinwahlen und das Vorspielen einer Demokratie je nach Ausmass Zeit. Jedoch ist es meist nur ein einzelner Parteivorsitzender, der entscheiden kann mit der Unterstützung seiner Partei, was den Prozess wiederum beschleunigt. Alles in allem bringt uns das zu einer zwei auf der Skala.',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 3,
                            description:
                                'Es gibt eine Hierarchie, jedoch ist diese nur limitiert vorhanden. Die Mitglieder der Partei haben einen besseren Status als andere und können in vorteilhafte Positionen gebracht werden. Ansonsten gibt es keine religiöse oder militärische begründete Hierarchie. Viele Einheitsparteien geben ausserdem vor, kommunistisch zu sein. Diese Ideologie beruht darauf, dass alle gleich sind. Was ebenfalls auf eine Gleichheit aller hinweisen würde. Dies führt uns zu einer drei auf der Skala.',
                        },
                    ],
                    img: 'party_dictatorship.svg',
                },
            ],
        },
        {
            id: 4,
            name: 'Anderes',
            description: 'Ich bin ein Aussenseiter',
            governments: [
                {
                    id: 10,
                    title: 'Anarchie',
                    subtitle: 'Der freie Staat',
                    description:
                        'Die Anarchie ist eine sehr spezielle Staatsform, denn es ist die Abwesenheit einer Regierung. Das bedeutet, dass es keine Bürokratie gibt und keine offiziellen Gesetze. Alle Entscheidungen werden von der Gesellschaft direkt bestimmt. Die Anarchie wird bei allen Kategorien mit einer 0 eingeordnet, weil es eine sehr spezielle Staatsform ist, die in allem ein extrem ist. Es gibt bei einer Anarchie gar keine offiziellen Gesetze. Demnach gibt es also gar keine Staatsgewalt, es gibt keine gesetzliche Hierarchie, es gibt keine konservativen Gesetze, es gibt kein Law and Order und es hat die maximale Effizienz, da es keine Bürokratie gibt.',
                    properties: [
                        {
                            left_title: 'Mitsprache',
                            right_title: 'Staatsgewalt',
                            value: 0,
                            description: '',
                        },
                        {
                            left_title: 'Freiheiten',
                            right_title: 'Law and Order',
                            value: 0,
                            description: '',
                        },
                        {
                            left_title: 'Progressiv',
                            right_title: 'Konservativ',
                            value: 0,
                            description: '',
                        },
                        {
                            left_title: 'Hohe Effizienz',
                            right_title: 'Tiefe Effizienz',
                            value: 0,
                            description: '',
                        },
                        {
                            left_title: 'Gleichheit',
                            right_title: 'Hierarchie',
                            value: 0,
                            description: '',
                        },
                    ],
                    img: 'anarchy.svg',
                },
            ],
        },
    ],
};

const module = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};

export default module;
