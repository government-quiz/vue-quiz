import Vue from 'vue';
import Vuex, { MutationTree, ActionTree, GetterTree } from 'vuex';
import { Result } from '@/models/result';

Vue.use(Vuex);

interface State {
    results: Result[];
}

const mutations: MutationTree<State> = {
    setResults(currentState: State, results: Result[]) {
        currentState.results = results;
    },
};

const actions: ActionTree<State, any> = {};

const getters: GetterTree<State, any> = {
    results(currentState: State) {
        return currentState.results;
    },
};

const state: State = {
    results: [
        {
            id: 0,
            government: {
                id: 0,
                title: '',
                description: '',
                subtitle: '',
                img: '',
                properties: [
                    {
                        left_title: '',
                        right_title: '',
                        value: 0,
                        description: '',
                    },
                ],
            },
            value: 0,
        },
    ],
};

const module = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};

export default module;
